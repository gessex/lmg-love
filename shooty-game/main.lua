--a couple of global variables as shortcuts for getting window dimensions
FrameWidth, FrameHeight = love.graphics.getDimensions()
--local variables declared outside of love.load so that every function can see them
local player
local enemy
local speedBoost
local bullets
local score

--load the player image and setup the player object
local playerImg = love.graphics.newImage("player.png")
function createPlayer()
  local player = {
    x = FrameWidth/2, --starting X position
    y = FrameHeight/2, --stating Y position
    originx = playerImg:getWidth()/2, --setting the draw origin to the center of the player image, so that rotation works properly
    originy = playerImg:getHeight()/2,--setting the draw origin to the center of the player image, so that rotation works properly
    radius = playerImg:getWidth()/2,
    rotation = 0,
    speed = 400,
    img = playerImg
  }
  return player
end


function createEnemy()
  local randomAngle = love.math.random()*(2*math.pi) -- 2*pi is 360 degrees
  local enemy = {
    x = (FrameWidth/2) + (500 * math.cos(randomAngle)), --start offscreen at a random angle relative to the center of the frame
    y = (FrameHeight/2) + (500 * math.sin(randomAngle)),--start offscreen at a random angle relative to the center of the frame
    radius = 10,
    speed = 100 + speedBoost
  }
  return enemy
end

--setting or resetting the starting values  
function initialize()
  player = createPlayer()
  bullets = {} --create a new bullets table; has the effect of overwriting the existing bullets table
  laser = {mx=player.x,my=player.y,life = 0.2}
  speedBoost = 0
  enemy = createEnemy()
  score = 0
end

function shoot()
  local bullet = {
    x = player.x,
    y = player.y,
    radius = 3,
    speed = 700,
    dir = player.rotation
  }
  table.insert(bullets,bullet)
end

--determine if two circles overlap
function hit(x1,y1,r1,x2,y2,r2)
  return (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
end

--destroy bullets that go out of frame
function OutOfBounds(x,y)
  return x < 0 or
  x > FrameWidth or
  y < 0 or
  y > FrameHeight
end

function scored()
  speedBoost = speedBoost + 20 -- make the enemy faster
  enemy = createEnemy() -- recreate enemy
  score = score + 1
end

function love.keypressed(key)
  if key == "space" then
    shoot()
  end
end

function love.load()
  initialize()
end

function love.update(dt)
  --rotate player toward mouse coordinates
  local aim = math.atan2(love.mouse.getY()-player.y,love.mouse.getX() - player.x)
  player.rotation = aim
  
  --make enemy chase player
  local dx = player.x - enemy.x --x component of distance between player and enemy
  local dy = player.y - enemy.y --y component of distance between player and enemy
  local angle = math.atan2(dy,dx)
  enemy.x = enemy.x + enemy.speed * math.cos(angle) * dt --move enemy toward player
  enemy.y = enemy.y + enemy.speed * math.sin(angle) * dt --never forget to multiply motion/physics by dt!
  
  --restart the game if the player is hit by an enemy
  if hit(enemy.x,enemy.y,enemy.radius,player.x,player.y,player.radius) then
    initialize()
  end
  
  for b,bullet in ipairs(bullets) do
    bullet.x = bullet.x + bullet.speed * math.cos(bullet.dir) * dt
    bullet.y = bullet.y + bullet.speed * math.sin(bullet.dir) * dt
    --if a bullet hits the enemy, destroy it and call the scored() function
    if hit(bullet.x,bullet.y,bullet.radius,enemy.x,enemy.y,enemy.radius) then
      table.remove(bullets,b)
      scored()
    end
    
    --destroy bullets that go out of frame
    if OutOfBounds(bullet.x,bullet.y) then
      table.remove(bullets,b)
    end
  end
  
  --player movement controls
  if love.keyboard.isDown("a") then
    player.x = player.x - player.speed * dt
  end
  if love.keyboard.isDown("d") then
    player.x = player.x + player.speed * dt
  end
  if love.keyboard.isDown("w") then
    player.y = player.y - player.speed * dt
  end
  if love.keyboard.isDown("s") then
    player.y = player.y + player.speed * dt
  end
end

function love.draw()
  --draw the player
  love.graphics.setColor(1,1,1)
  love.graphics.draw(player.img,player.x,player.y,player.rotation,1,1,player.originx,player.originy)
  
  --draw the enemy
  love.graphics.setColor(1,0,0)
  love.graphics.circle("fill",enemy.x,enemy.y,enemy.radius)
  
  --draw the bullets
  love.graphics.setColor(0,1,1)
  for b,bullet in ipairs(bullets) do
    love.graphics.circle("fill",bullet.x,bullet.y,bullet.radius)
  end
  
  --draw the score
  love.graphics.setColor(1,1,1)
  love.graphics.print("SCORE: "..tostring(score),10,10)
end
